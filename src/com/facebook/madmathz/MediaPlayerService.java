package com.facebook.madmathz;
import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MediaPlayerService extends Service implements OnCompletionListener {
	
	  MediaPlayer mediaPlayer;

	  @Override
	  public IBinder onBind(Intent intent) {
	    return null;
	  }

	  @Override
	  public void onCreate() {
	    mediaPlayer = MediaPlayer.create(this, R.raw.bgmusic);// raw/s.mp3
	    mediaPlayer.setOnCompletionListener(this);
	  }

		public void stopMusic()
		{
			 mediaPlayer.stop();
			 mediaPlayer.release();
			 mediaPlayer = null;
		}

	  @Override
	  public int onStartCommand(Intent intent, int flags, int startId) {
	    if (!mediaPlayer.isPlaying()) {
	      mediaPlayer.setLooping(true);
	      mediaPlayer.start();
	    }
	    return START_STICKY;
	  }

	  public void onDestroy() {
	    if (mediaPlayer.isPlaying()) {
	      mediaPlayer.stop();
	    }
	    mediaPlayer.release();
	  }

	  public void onCompletion(MediaPlayer _mediaPlayer) {
	    stopSelf();
	  }

}
