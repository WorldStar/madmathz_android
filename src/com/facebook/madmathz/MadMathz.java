package com.facebook.madmathz;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ScrollView;
import android.view.View;
import android.media.*;

import com.facebook.AccessToken;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.android.Util;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.madmathz.StartInGame;
import com.facebook.madmathz.GameView.MyImageView;
import com.google.android.gcm.GCMRegistrar;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
public class MadMathz extends Activity implements  OnClickListener{
	
	//Sound mSoundManager;
    MediaPlayer mMediaPlayerBackground;
	ImageButton mBtnFind;
	ImageButton mBtnStart;
	ImageButton mBtnSetUp;
	TextView mPoint;
	private StartInGame mStartInGame;
	ImageView imgLoadingView;
	RelativeLayout loadingView;
	//LinearLayout pointingView;
	
	//UserInfo
	ImageView thumbnail = null;
	TextView UserHelloText = null;
	TextView UserPointsText = null;
	
	int cTime = 80;
	boolean ableFlg = false;
//	public String mAbleFlg ;
	private SharedPreferences sharedPrefrences;
	private Editor editor;
	int yourPoint;
	
	public static final int LOAD_PROFILE = 1000;
	public static final int LOAD_PROFILE_FINISH = 2000;
	public static final int LOADING = 3000;
	public static final int LOADING_FINISH = 4000;
	public static final int LOAD_FRIENDS = 5000;
	public static final int LOAD_WEBSERVICE = 7000;
	
	public static boolean bLoading = false;
	public int state = 0;
	
	
	public void getChallengeData()
	{
		//get ChallengeList
		
	}
	
	
	public ArrayList<ChallengeList> mChallengeList = new ArrayList<ChallengeList>();
	public ArrayList<ChallengeList> mChallengeResult = new ArrayList<ChallengeList>();
	
	ListView listView;
	

	
	public void getChallengeResult()
	{
		String response;
		HashMap<String, String> params = new HashMap<String, String>();
		try
		{
			params.put("FBID", Preferences.FBID);
			//params.add(new BasicNameValuePair("FBID", Preferences.FBID));
			response = HttpManager.postHttpResponse(URI.create(Preferences.selectChallengeResultByFBID),  params);
			
			if(response != "") 
			{
				
				System.out.println(response);
				JSONObject json = new JSONObject(response);
				JSONArray challenge_list = json.getJSONArray("posts");
				mChallengeResult.clear();
				for(int i = 0; i < challenge_list.length(); i++)
				{
					ChallengeList t_list = new ChallengeList();
					
					JSONObject user = challenge_list.getJSONObject(i).getJSONObject("user");
					t_list.id = user.getString("id");
					t_list.fplayer_id = user.getString("fplayer_id");
					t_list.splayer_id = user.getString("splayer_id");
					t_list.fplayer_msg = user.getString("fplayer_msg");
					t_list.splayer_msg = user.getString("splayer_msg");
					t_list.fplayer_comp_time = user.getInt("fplayer_comp_time");
					t_list.splayer_comp_time = user.getInt("splayer_comp_time");
					t_list.fplayer_comp_poss = user.getInt("fplayer_comp_poss");
					t_list.splayer_comp_poss = user.getInt("splayer_comp_poss");
					t_list.match_played_time = user.getString("match_played_time");
					t_list.fplayer_wrong_tried = user.getInt("fplayer_wrong_tried");
					t_list.splayer_wrong_tried = user.getInt("splayer_wrong_tried");
					t_list.gameplay_id = user.getString("gameplay_id");
					t_list.match_id = user.getString("match_id");
					t_list.status = user.getInt("status");
					t_list.fplayer_username = user.getString("fplayer_username");
					t_list.splayer_username = user.getString("splayer_username");
					t_list.fplayer_repeat = user.getInt("fplayer_repeat");
					t_list.splayer_repeat = user.getInt("splayer_repeat");
					t_list.result = user.getInt("result");
					
					mChallengeResult.add(t_list);
				}
				
				mHandler.post(new Runnable(){

					@Override
					public void run() {
						// TODO Auto-generated method stub
						
						challengeresult_update();
					}});
				
			}
			
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	public void challengeresult_update()
	{
		LinearLayout challengelist = (LinearLayout)findViewById(R.id.ChallengeResultList);
		challengelist.removeAllViews();
	    LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View inflatedView = mInflater.inflate(R.layout.challengeresult_header, null);
	    
		challengelist.addView(inflatedView);
		
		if(this.mChallengeResult.size() == 0)
		{
			challengelist.setVisibility(View.GONE);
			return;
		}
		
		challengelist.setVisibility(View.VISIBLE);
		
		for(int i = 0; i < mChallengeResult.size(); i++)
		{
			ChallengeList user = mChallengeResult.get(i);
			View item = mInflater.inflate(R.layout.challengeresultitem, null);
			final ImageView thumbnail = (ImageView)item.findViewById(R.id.ResultUserThumbnail);
			TextView name = (TextView)item.findViewById(R.id.ResultUserName);
			name.setTextColor(Preferences.tx_color);
			name.setTypeface(Preferences.tf_bold, Typeface.BOLD);
			name.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
			
			TextView ChallengeTime = (TextView)item.findViewById(R.id.ResultChallengeTime);
			ChallengeTime.setTextColor(Preferences.tx_color_red);
			ChallengeTime.setTypeface(Preferences.tf_normal);
			ChallengeTime.setTextSize(TypedValue.COMPLEX_UNIT_SP, 11);
			
			TextView PostTime = (TextView)item.findViewById(R.id.ResultPostTime);
			PostTime.setTextColor(Preferences.tx_color);
			PostTime.setTypeface(Preferences.tf_bold);
			PostTime.setTextSize(TypedValue.COMPLEX_UNIT_SP, 11);
			
			ImageButton btn = (ImageButton)item.findViewById(R.id.ResultChallengeBtn);
			
			if(user.fplayer_id.equals(Preferences.FBID))
			{
				thumbnail.setTag(String.format(Preferences.FacebookPicUrl, user.splayer_id));
				//thumbnail.setImageURI(Uri.parse(String.format(Preferences.FacebookPicUrl, user.splayer_id)));
				name.setText(user.splayer_username);
				PostTime.setText(user.splayer_msg);
				
				if(user.fplayer_comp_poss > user.splayer_comp_poss)
				{
					ChallengeTime.setText(String.format("You win! %d scores more.", user.fplayer_comp_poss - user.splayer_comp_poss));
				}
				else if(user.fplayer_comp_poss < user.splayer_comp_poss)
				{
					ChallengeTime.setText(String.format("You lose! %d scores less.", user.splayer_comp_poss - user.fplayer_comp_poss));
				}
				else
				{
					if(user.fplayer_comp_time < user.splayer_comp_time)
					{
						ChallengeTime.setText(String.format("You win! %ds faster.", user.splayer_comp_time - user.fplayer_comp_time));
					}
					else if(user.fplayer_comp_time > user.splayer_comp_time)
					{
						ChallengeTime.setText(String.format("You lose! %ds slower.", user.fplayer_comp_time - user.splayer_comp_time));
					}
					else
					{
						if(user.fplayer_wrong_tried < user.splayer_wrong_tried)
						{
							ChallengeTime.setText(String.format("You win! %d less wrong.", user.splayer_wrong_tried - user.fplayer_wrong_tried));
						}
						else if(user.fplayer_wrong_tried > user.splayer_wrong_tried)
						{
							ChallengeTime.setText(String.format("You lose! %d more wrong.", user.fplayer_wrong_tried - user.splayer_wrong_tried));
						}
						else
						{
							if(user.fplayer_repeat < user.splayer_repeat)
							{
								ChallengeTime.setText(String.format("You win! %d less repeat.", user.splayer_repeat - user.fplayer_repeat));
							}
							else if(user.fplayer_repeat > user.splayer_repeat)
							{
								ChallengeTime.setText(String.format("You lose! %d more repeat.", user.fplayer_repeat - user.splayer_repeat));
								
							}
							else
							{
								ChallengeTime.setText("Draw !!");
							}
						}
					}
				}
				
				//btn.setBackgroundResource(R.drawable.btn_waiting);
				//btn.setImageResource(R.drawable.btn_waiting);
				//btn.setEnabled(false);
			}
			else
			{
				thumbnail.setTag(String.format(Preferences.FacebookPicUrl, user.fplayer_id));
				//thumbnail.setImageURI(Uri.parse(String.format(Preferences.FacebookPicUrl, user.splayer_id)));
				name.setText(user.fplayer_username);
				PostTime.setText(user.fplayer_msg);
				
				if(user.fplayer_comp_poss > user.splayer_comp_poss)
				{
					ChallengeTime.setText(String.format("You lose! %d score less.", user.fplayer_comp_poss - user.splayer_comp_poss));
				}
				else if(user.fplayer_comp_poss < user.splayer_comp_poss)
				{
					ChallengeTime.setText(String.format("You win! %d score more.", user.splayer_comp_poss - user.fplayer_comp_poss));
				}
				else
				{
					if(user.fplayer_comp_time < user.splayer_comp_time)
					{
						ChallengeTime.setText(String.format("You lose! %ds slower.", user.splayer_comp_time - user.fplayer_comp_time));
					}
					else if(user.fplayer_comp_time > user.splayer_comp_time)
					{
						ChallengeTime.setText(String.format("You win! %ds faster", user.fplayer_comp_time - user.splayer_comp_time));
					}
					else
					{
						if(user.fplayer_wrong_tried < user.splayer_wrong_tried)
						{
							ChallengeTime.setText(String.format("You lose! %d more wrong.", user.splayer_wrong_tried - user.fplayer_wrong_tried));
						}
						else if(user.fplayer_wrong_tried > user.splayer_wrong_tried)
						{
							ChallengeTime.setText(String.format("You win! %d less wrong.", user.fplayer_wrong_tried - user.splayer_wrong_tried));
						}
						else
						{
							if(user.fplayer_repeat < user.splayer_repeat)
							{
								ChallengeTime.setText(String.format("You lose! %d more repeat.", user.splayer_repeat - user.fplayer_repeat));
							}
							else if(user.fplayer_repeat > user.splayer_repeat){
								ChallengeTime.setText(String.format("You win! %d less repeat.", user.fplayer_repeat - user.splayer_repeat));
							}
							else
							{
								ChallengeTime.setText("Draw !!");
							}
						}
					}
				}
			}
			
			mHandler.post(new Runnable(){

				@Override
				public void run() {
					// TODO Auto-generated method stub
					new GetPicAsyncTask().execute(thumbnail);
				}
				
			});
			
			//btn.setBackgroundResource(R.drawable.btn_rechallenge);
			btn.setImageResource(R.drawable.btn_rechallenge);
			btn.setOnTouchListener(new ButtonHighlight(btn));
			btn.setTag(i);

			challengelist.addView(item);
			
			
			if(i < mChallengeResult.size() - 1)
			{
				View view = new View(this);
				LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
			            (LinearLayout.LayoutParams.FILL_PARENT), 2);
				view.setLayoutParams(lp);
				view.setBackgroundColor(Preferences.tx_color);
				
				challengelist.addView(view);
			}
		}
		
		challengelist.invalidate();
	}
	
	public void challengelist_update()
	{
		
		LinearLayout challengelist = (LinearLayout)findViewById(R.id.challengeList);
		challengelist.removeAllViews();
		
	    LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View inflatedView = mInflater.inflate(R.layout.challengelist_header, null);
	    
		challengelist.addView(inflatedView);
		
		if(mChallengeList.size() == 0)
		{
			challengelist.setVisibility(View.GONE);
			return;
		}
		
		challengelist.setVisibility(View.VISIBLE);
		
		for(int i = 0; i < mChallengeList.size(); i++)
		{
			ChallengeList user = mChallengeList.get(i);
			View item = mInflater.inflate(R.layout.challengelistitem, null);
			final ImageView thumbnail = (ImageView)item.findViewById(R.id.UserThumbnail);
			TextView name = (TextView)item.findViewById(R.id.UserName);
			name.setTextColor(Preferences.tx_color);
			name.setTypeface(Preferences.tf_bold, Typeface.BOLD);
			name.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
			
			TextView ChallengeTime = (TextView)item.findViewById(R.id.ChallengeTime);
			ChallengeTime.setTextColor(Preferences.tx_color_red);
			ChallengeTime.setTypeface(Preferences.tf_normal);
			ChallengeTime.setTextSize(TypedValue.COMPLEX_UNIT_SP, 11);
			
			TextView PostTime = (TextView)item.findViewById(R.id.PostTime);
			PostTime.setTextColor(Preferences.tx_color);
			PostTime.setTypeface(Preferences.tf_bold);
			PostTime.setTextSize(TypedValue.COMPLEX_UNIT_SP, 11);
			
			ImageButton btn = (ImageButton)item.findViewById(R.id.ChallengeBtn);
			
			thumbnail.setTag(String.format(Preferences.FacebookPicUrl, user.fplayer_id));
			name.setText(user.fplayer_username);
			
			if(user.fplayer_id.equals(Preferences.FBID))
			{
				//thumbnail.setImageURI(Uri.parse(String.format(Preferences.FacebookPicUrl, user.splayer_id)));
				ChallengeTime.setText(String.format("You had hit %d scores in %d seconds", user.fplayer_comp_poss, user.fplayer_comp_time));
				//btn.setBackgroundResource(R.drawable.btn_waiting);
				btn.setImageResource(R.drawable.btn_waiting);
				btn.setEnabled(false);
			}
			else
			{
				//thumbnail.setTag(String.format(Preferences.FacebookPicUrl, user.fplayer_id));
				//name.setText(user.fplayer_username);
				ChallengeTime.setText(String.format("had hit %d scores in %d seconds", user.fplayer_comp_poss, user.fplayer_comp_time));
				btn.setImageResource(R.drawable.btn_accept);
				btn.setOnTouchListener(new ButtonHighlight(btn));
				btn.setTag(i);
			}
			
			mHandler.post(new Runnable(){

				@Override
				public void run() {
					// TODO Auto-generated method stub
					new GetPicAsyncTask().execute(thumbnail);
				}
				
			});
			
			
			int tempDifferent = user.DiffTime;
			tempDifferent = tempDifferent / 60;
			
			if(tempDifferent == 0)
			{
				PostTime.setText("1 minute ago");
			}
			else if(tempDifferent < 60)
			{
				PostTime.setText(String.format("%d minutes ago", tempDifferent));
			}
			else if(tempDifferent > 60)
			{
				tempDifferent = tempDifferent / 60;
				
				if(tempDifferent < 2)
				{
					PostTime.setText(String.format("%d hour ago", tempDifferent));
				}
				else
				{
					PostTime.setText(String.format("%d hours ago", tempDifferent));
				}
			}
			challengelist.addView(item);
			
			
			if(i < mChallengeList.size() - 1)
			{
				View view = new View(this);
				LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
			            (LinearLayout.LayoutParams.FILL_PARENT), 2);
				view.setLayoutParams(lp);
				view.setBackgroundColor(Preferences.tx_color);
				
				challengelist.addView(view);
			}
		}
		
		challengelist.invalidate();
	}
	
	
	public void getChallengeList()
	{
		
		String response;
		HashMap<String, String> params = new HashMap<String, String>();
		try
		{
			params.put("FBID", Preferences.FBID);
//			params.add(new BasicNameValuePair("FBID", Preferences.FBID));
			response = HttpManager.postHttpResponse(URI.create(Preferences.selectChallengeByFBID),  params);
			
			if(response != "") 
			{
				System.out.println(response);
				
				JSONObject json = new JSONObject(response);
				JSONArray challenge_list = json.getJSONArray("posts");
				mChallengeList.clear();
				for(int i = 0; i < challenge_list.length(); i++)
				{
					ChallengeList t_list = new ChallengeList();
					
					JSONObject t_challenge_list = challenge_list.getJSONObject(i).getJSONObject("user");
					
					t_list.id = t_challenge_list.getString("id");
				
					t_list.fplayer_id = t_challenge_list.getString("fplayer_id");
					t_list.splayer_id = t_challenge_list.getString("splayer_id");
					t_list.fplayer_msg = t_challenge_list.getString("fplayer_msg");
					t_list.splayer_msg = t_challenge_list.getString("splayer_msg");
					t_list.fplayer_comp_time = t_challenge_list.getInt("fplayer_comp_time");
					t_list.splayer_comp_time = t_challenge_list.getInt("splayer_comp_time");
					t_list.fplayer_comp_poss = t_challenge_list.getInt("fplayer_comp_poss");
					t_list.splayer_comp_poss = t_challenge_list.getInt("splayer_comp_poss");
					t_list.match_played_time = t_challenge_list.getString("match_played_time");
					t_list.fplayer_wrong_tried = t_challenge_list.getInt("fplayer_wrong_tried");
					t_list.splayer_wrong_tried = t_challenge_list.getInt("splayer_wrong_tried");
					t_list.gameplay_id = t_challenge_list.getString("gameplay_id");
					t_list.match_id = t_challenge_list.getString("match_id");
					t_list.status = t_challenge_list.getInt("status");
					t_list.fplayer_username = t_challenge_list.getString("fplayer_username");
					t_list.splayer_username = t_challenge_list.getString("splayer_username");
					t_list.fplayer_repeat = t_challenge_list.getInt("fplayer_repeat");
					t_list.splayer_repeat = t_challenge_list.getInt("splayer_repeat");
					t_list.result = t_challenge_list.optInt("result");
					t_list.DiffTime = t_challenge_list.getInt("DiffTime");
					
					mChallengeList.add(t_list);
				}
				
				mHandler.post(new Runnable(){

					@Override
					public void run() {
						// TODO Auto-generated method stub
						challengelist_update();
						
					}});
		}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	//callback for challengelist buttons
	public void ChallengeListListener(View v)
	{
		int position= (Integer) v.getTag();
		
		ChallengeList t_list = this.mChallengeList.get(position);
		
		//if the current user is first player, return
		if(Preferences.FBID.equals(t_list.fplayer_id)) return;
		
		//accept Game
		
		Preferences.cur_ChallengeList = t_list;
		Preferences.cur_ChallengeList.GameType = StartInGame.TYPE_ACCEPTGAME;
		
		//GameInfo int
		Intent intent = new Intent(MadMathz.this, StartInGame.class);
		intent.putExtra("GameState", StartInGame.STATE_START);
  	    intent.putExtra("setImgView" , "startGameState");
		startActivity(intent);
		isContinue = true;
		MadMathz.this.finish();
	}
	
	public boolean isContinue = false;
	
	protected void onDestroy()
	{
		mHandler.removeCallbacks(GetUserInfo);
		super.onDestroy();
	}
	
	//callback for challengeresult buttons
	public void ChallengeResultListener(View v)
	{
		int position = (Integer) v.getTag();
		
		ChallengeList t_list = this.mChallengeResult.get(position);
		
		//gameinfo init
		ChallengeList t_user = new ChallengeList();
		
		t_user.id = t_list.id;
		t_user.fplayer_id = Preferences.FBID;
		t_user.fplayer_username = Preferences.UserName;
		t_user.GameType = StartInGame.TYPE_RECHALLENGE;
		
		
		if(t_list.fplayer_id.equals(Preferences.FBID))
		{
			t_user.splayer_id = t_list.splayer_id;
			t_user.splayer_username = t_list.splayer_username;
		}else
		{
			t_user.splayer_id = t_list.fplayer_id;
			t_user.splayer_username = t_list.fplayer_username;
		}
		
		Preferences.cur_ChallengeList = t_user;
		
		Intent intent = new Intent(MadMathz.this, StartInGame.class);
		intent.putExtra("GameState", StartInGame.STATE_START);
  	    intent.putExtra("setImgView" , "startGameState");
		startActivity(intent);
		isContinue = true;
		MadMathz.this.finish();
	}
	
	public String AllUserInfo;
	

	
	// WebService SelectScoreByFBID
	public void getAllUserInfo()
	{
		String response;
		try
		{
			AllUserInfo = HttpManager.getHttpResponse(URI.create(Preferences.selectAllUserFBIDAndScore));
		}catch(Exception e)
		{
			
		}
			

	}
	
	// WebService SelectScoreByFBID
	public void getSelectScoreByFBID()
	{
		if(Preferences.isIncomingCall || !Utility.isNetworkReacheable(this))
		{
			loadingView.setVisibility(View.INVISIBLE);
			return;
		}
		String response;
		HashMap<String, String> params = new HashMap<String, String>();
		try
		{
			params.put("FBID", Preferences.FBID);
//			params.add(new BasicNameValuePair("FBID", Preferences.FBID));
			response = HttpManager.postHttpResponse(URI.create(Preferences.selectUserScoreByFBID),  params);
			JSONArray json_arr = new JSONObject(response).getJSONArray("posts");
			
			
			//parse SelectScore
			
			if(json_arr.length() == 0) //new User
			{
				params.clear();
				params.put("FirstName", Preferences.FirstName);
				params.put("LastName", Preferences.LastName);
				params.put("Gender", Preferences.Gender);
				params.put("Email", Preferences.Email);
				params.put("FBUsername", Preferences.UserName);
				params.put("FBID", Preferences.FBID);
				params.put("PushToken", Preferences.PushToken);  //should implement the Push Token logic
				params.put("device_type", "android");
				
				response = HttpManager.postHttpResponse(URI.create(Preferences.insertNewUser), params);
				
			}
			else
			{
				Preferences.Score = json_arr.getJSONObject(0).getJSONObject("user").getInt("score");
			}
			//register GCM PUSH TOKEN
			
			if(Preferences.PushToken.equals(""))
			{
				initGcmReg();
			}
			else
			{
				params.clear();
				params.put("PushToken", Preferences.PushToken);
				params.put("FBID", Preferences.FBID);
				params.put("device_type", "android");
				response = HttpManager.postHttpResponse(URI.create(Preferences.updateUserPushToken), params);
			}
			
			
			//get All user score and fb_id
			String users_response = HttpManager.getHttpResponse(URI.create(Preferences.selectAllUserFBIDAndScore));
			
			if(users_response != "")
			{
				JSONArray json = new JSONObject(users_response).getJSONArray("posts");
				
				if(json.length() > 0)
				{
					Preferences.AllUserInfo.clear();
					
					for(int i = 0; i < json.length(); i++)
					{
						JSONObject json_user = json.getJSONObject(i).getJSONObject("user");
						String fb_id = json_user.getString("fb_id");
						String score = json_user.getString("score");
						
						Preferences.AllUserInfo.put(fb_id,  score);
					}
					
				}
				
			}
		}
		catch(Exception e)
		{
		}
	}
	
	private class getFBFriendsList extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			if(Preferences.isIncomingCall || ! Utility.isNetworkReacheable(MadMathz.this))
			{
				loadingView.setVisibility(View.INVISIBLE);
				return "";
			}
			String response = HttpManager.getHttpResponse(URI.create(String.format(Preferences.FacebookFriendProfileUrl, Preferences.access_token)));
			return response;
		}
		
		 @Override
		   protected void onPostExecute(String response) {   
			 

		    	friendlist_str = response;
		        Log.d("Profile", response);
		        String json = response;
		        
		        if(response == "") return;
		        try {
					//structure id, name, picture
		        	JSONArray friends_list = new JSONObject(json).getJSONArray("data");
		        	Preferences.mFriends_List.clear();
		        	
		        	for(int i = 0; i < friends_list.length(); i++)
		        	{
		        		HashMap<String, String> map = new HashMap<String, String>();
		        		map.put("id", friends_list.getJSONObject(i).getString("id"));
		        		map.put("name", friends_list.getJSONObject(i).getString("name"));
		        		map.put("picture", friends_list.getJSONObject(i).getJSONObject("picture").getJSONObject("data").getString("url"));
		        		Preferences.mFriends_List.add(map);
		        	}
		        	
		        	mHandler.post(GetUserInfo);
					//new getUserInformation().execute("");
		        } catch (JSONException e) {
		            e.printStackTrace();
		        }
			 
		 }
		
	}
	
	boolean isUserDataAvailable = false;
	 private Runnable GetUserInfo = new Runnable() {
	        public void run() {
        		new getUserInformation().execute("");
	        }
	    };
	
	
	private class getUserInformation extends AsyncTask<String, Void, String>
	{

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			isUserDataAvailable = true;
			getSelectScoreByFBID();  //also include sending new user data
			getChallengeList();
			getChallengeResult();
			
			if(Preferences.isIncomingCall) return null;
			
			mHandler.post(new Runnable(){

				@Override
				public void run() {
					// TODO Auto-generated method stub
					Utility.removeLoading(loadingView);
				}});
			
			if(!bStop)mHandler.postDelayed(GetUserInfo, 30000);

			bLoading = false;

			return null;
		}
		
	}
	
	private class getFBInformation extends AsyncTask<String, Void, String> {
		
		public Bundle game_info = new Bundle();
		public String gameplay_response = "";

	    @Override
	    protected String doInBackground(String... params) {
	        	//Http Request
	    	if(Preferences.isIncomingCall || ! Utility.isNetworkReacheable(MadMathz.this))
	    	{
	    		loadingView.setVisibility(View.INVISIBLE);
	    		return "";
	    	}
			String response = HttpManager.getHttpResponse(URI.create(String.format(Preferences.FacebookUserProfileUrl, Preferences.access_token)));

	        return response;
	    }      

	    @Override
	    protected void onPostExecute(String response) {   

	        JSONObject profile;
	        if(response == "") return;
			try {
				profile = new JSONObject(response);

		        Preferences.FBID = profile.getString("id");
		        Preferences.Email = profile.getString("email");
		        Preferences.LastName = profile.getString("last_name");
		        Preferences.FirstName = profile.getString("first_name");
		        Preferences.UserName = profile.getString("name");
		        Preferences.Gender = profile.getString("gender");
		        Preferences.UpdatedTime = profile.getString("updated_time"); 
//			    	        Preferences.Verified = profile.getBoolean("verified");
		        Preferences.UserPic = "http://graph.facebook.com/"+ Preferences.FBID +"/picture";
//	    	        Preferences.mUserPic = Utility.getBitmap(Preferences.UserPic);
		        thumbnail.setTag(Preferences.UserPic);
    	        new GetPicAsyncTask().execute(thumbnail);
		        
		        Preferences.savePreferences(getApplicationContext());
		        
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			new getFBFriendsList().execute("");
	 
	    }
	}
	
	
	
	//Facebook User Information
	public void getFBInformation()
	{
		if(!Preferences.isIncomingCall || !Utility.isNetworkReacheable(this))
		{
			loadingView.setVisibility(View.INVISIBLE);
			return;
		}
		String response = HttpManager.getHttpResponse(URI.create(String.format(Preferences.FacebookUserProfileUrl, Preferences.access_token)));

        JSONObject profile;
		try {
			profile = new JSONObject(response);

	        Preferences.FBID = profile.getString("id");
	        Preferences.Email = profile.getString("email");
	        Preferences.LastName = profile.getString("last_name");
	        Preferences.FirstName = profile.getString("first_name");
	        Preferences.UserName = profile.getString("name");
	        Preferences.Gender = profile.getString("gender");
	        Preferences.UpdatedTime = profile.getString("updated_time"); 
//		    	        Preferences.Verified = profile.getBoolean("verified");
	        Preferences.UserPic = "http://graph.facebook.com/"+ Preferences.FBID +"/picture";
//    	        Preferences.mUserPic = Utility.getBitmap(Preferences.UserPic);
	        thumbnail.setTag(Preferences.UserPic);
	        mHandler.post(new Runnable(){
	        	public void run()
	        	{
	    	        new GetPicAsyncTask().execute(thumbnail);
	        	}
	        });
	        
	        Preferences.savePreferences(getApplicationContext());
	        
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 
   	mHandler.post(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				getFBFriendslist();
				
			}});
	}

	public String friendlist_str;

	//Facebook FriendList
	public void getFBFriendslist()
	{
		if(Preferences.isIncomingCall || ! Utility.isNetworkReacheable(this))
		{
			loadingView.setVisibility(View.INVISIBLE);
			return;
		}
		String response = HttpManager.getHttpResponse(URI.create(String.format(Preferences.FacebookFriendProfileUrl, Preferences.access_token)));

    	friendlist_str = response;
        Log.d("Profile", response);
        String json = response;
        try {
			//structure id, name, picture
        	JSONArray friends_list = new JSONObject(json).getJSONArray("data");
        	Preferences.mFriends_List.clear();
        	
        	for(int i = 0; i < friends_list.length(); i++)
        	{
        		HashMap<String, String> map = new HashMap<String, String>();
        		map.put("id", friends_list.getJSONObject(i).getString("id"));
        		map.put("name", friends_list.getJSONObject(i).getString("name"));
        		map.put("picture", friends_list.getJSONObject(i).getJSONObject("picture").getJSONObject("data").getString("url"));
        		Preferences.mFriends_List.add(map);
        	}
        	
        	/*
        	Message msg = new Message();
        	msg.what = LOAD_WEBSERVICE;
            mHandler.sendMessage(msg);
            */
        	
        	mHandler.post(new Runnable(){

				@Override
				public void run() {
					// TODO Auto-generated method stub
					getSelectScoreByFBID();  //also include sending new user data
					getChallengeList();
					getChallengeResult();
					Utility.removeLoading(loadingView);
				}});
        } catch (JSONException e) {
            e.printStackTrace();
        }
	}
	
	
	
	
	public Handler mHandler=new Handler()  
    {  
        public void handleMessage(Message msg)  
        {  
        	/*
        	LoadThread loadProcess;
            switch(msg.what)  
            {  
	            case LOADING:
	            	bLoading = true;
	            	state = LOADING;
	            	loadProcess = new LoadThread();
	            	loadProcess.run();
	            	break;
	            
	            case LOAD_FRIENDS:
	            	state = LOAD_FRIENDS;
	            	loadProcess = new LoadThread();
	            	loadProcess.run();
	            	break;
	            	
	            case LOAD_WEBSERVICE:
	            	state = LOAD_WEBSERVICE;
	            	loadProcess = new LoadThread();
	            	loadProcess.run();
	            	break;
	            	
	            case LOADING_FINISH:
	            	Utility.removeLoading(loadingView);
//	            	loadingView.setVisibility(View.GONE);
	            	//loadingView.removeView(loadview);
	            	bLoading = false;
	            	break;
	            default:  
	                break;            
            } 
            */
            if(bLoading == true){
            	loadingView.invalidate();
            }
           // my_layout.invalidate();  
            super.handleMessage(msg);  
        }  
    };  
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	    Display localDisplay = this.getWindowManager().getDefaultDisplay();
		setContentView(R.layout.activity_mad_mathz);
		
		Intent intent = getIntent();
		//mAbleFlg = intent.getStringExtra("ableFlag");
		mBtnFind = (ImageButton)findViewById(R.id.mBtnFind);
		mBtnStart = (ImageButton)findViewById(R.id.mBtnStart);
		mBtnSetUp = (ImageButton)findViewById(R.id.mBtnSetUp);


        //UserInfo Settings
		thumbnail = (ImageView)findViewById(R.id.thumbnail);
		UserHelloText = (TextView)findViewById(R.id.UserHelloText);
		UserPointsText = (TextView)findViewById(R.id.UserPointsText);
		UserHelloText.setTypeface(Preferences.tf_normal,Typeface.BOLD);  
		UserPointsText.setTypeface(Preferences.tf_normal);
		UserHelloText.setTextColor(Preferences.tx_color);
		UserPointsText.setTextColor(Preferences.tx_color);
		UserPointsText.setText(String.format("You have %d points", Preferences.Score));
		mBtnFind.setOnClickListener(this);
		mBtnStart.setOnClickListener(this);
		mBtnSetUp.setOnClickListener(this);
		mBtnFind.setOnTouchListener(new ButtonHighlight(mBtnFind));
		mBtnStart.setOnTouchListener(new ButtonHighlight(mBtnStart));
		mBtnSetUp.setOnTouchListener(new ButtonHighlight(mBtnSetUp));
		
		//init loadingView
		this.loadingView = (RelativeLayout) findViewById(R.id.imgLoadingView);
	    bLoading = true;
	    
	    startGame();
	   
	}
	
	public void startGame()
	{
		 mHandler.post(new Runnable(){

				@Override
				public void run() {
					// TODO Auto-generated method stub
	            	Utility.drawLoading(loadingView, MadMathz.this);
				}});
	    if(!Utility.isNetworkReacheable(this))
	    {
	    	Utility.removeLoading(loadingView);
	    }else
	    {
		    new getFBInformation().execute("");
	    }
	}
	
	String reg_id = null;
	
	public void initGcmReg()
	{
		check_existence_reg_id ch = new check_existence_reg_id(Preferences.PushToken);	//check_existence_reg_id클래스는 reg_id의 존재여부를 파악.
		
		if(ch.check()) //참이면 reg_id가 등록이 되있다는 것. 
		{
			System.out.println("Registered");
		}
		else
		{
			GCMRegistration_id();		//reg_id 등록 함수 호출 
		}
	}
	
	
	public void GCMRegistration_id()
	{
		
		GCMRegistrar.checkDevice(this);
		GCMRegistrar.checkManifest(this);
		
		
		
		Preferences.PushToken = GCMRegistrar.getRegistrationId(this);
		
		//Log.i(TAG, "registration id =====&nbsp; "+regId);
		
		if (Preferences.PushToken.equals("")) {
			GCMRegistrar.register(getApplicationContext(), Preferences.SENDER_ID);
		}else{
		}

	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_mad_mathz, menu);
		return true;
	}
 public void onClick(View v) {
	 
	 if(bLoading == true) return;
	 if(Preferences.isIncomingCall) return;
	 
	 	if(mBtnFind.equals(v)){
	 		
	 		if(!Utility.isNetworkReacheable(this))
	 		{
	 			Utility.showToast(this, Preferences.TOAST_NOCONNECT);
	 			return;
	 		}
	 		
	 		getAllUserInfo();
			Intent intent = new Intent(this, FriendsList.class);
			intent.putExtra("ALL_USERS", this.AllUserInfo);
			intent.putExtra("API_RESPONSE", friendlist_str);
		    startActivity(intent);  
		    isContinue = true;
		    this.finish();
		}
		 if(mBtnStart.equals(v)){
			 
			 if(!Utility.isNetworkReacheable(this))
			 {
				 Utility.showToast(this, Preferences.TOAST_NOCONNECT);
				 return;
			 }

			 ChallengeList t_user = new ChallengeList();
			 t_user.fplayer_id = Preferences.FBID;
			 t_user.fplayer_username = Preferences.UserName;
			 t_user.GameType = StartInGame.TYPE_SINGLEGAME;
			 
			 Preferences.cur_ChallengeList = t_user;
			
			 Intent intent = new Intent(this, StartInGame.class);		 
			 intent.putExtra("GameState", StartInGame.STATE_START);
		     startActivity(intent);  
		     isContinue = true;
		     this.finish();
		 }
		 if(mBtnSetUp.equals(v)){
			 Intent intent = new Intent(this, Setting.class);		 
			 //intent.putExtra("setImgView" , "startGameState");
		     //startActivityForResult(intent, RESULT_SETTING); 
			 startActivity(intent);
			 isContinue = true;
			 this.finish();
		 }
	 }
 
 
 	public static final int RESULT_SETTING = 1;
 	public static final int DATA_NORETURN = 2;
 	public static final int DATA_RETURN = 3;
 
/*
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if(keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_HOME || keyCode == KeyEvent.KEYCODE_MENU)
		{
			stopService(new Intent(this, MediaPlayerService.class));
			Preferences.savePreferences(getApplicationContext());
			this.finish();
			return true;
		}
	
		//return super.onKeyDown(keyCode, event);	
		return false;
	}
	*/
	
	
	protected void onResume()
	{
		super.onResume();
		bStop = false;

		if(Preferences.isIncomingCall)
		{
			Preferences.isIncomingCall = false;
			if(Preferences.bBGM) startService(new Intent(this, MediaPlayerService.class));
			
			mHandler.post(new Runnable(){ //call process

				@Override
				public void run() {
					// TODO Auto-generated method stub
					
			    	LayoutInflater mInflater = (LayoutInflater) MadMathz.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			    	View inflatedView = mInflater.inflate(R.layout.phone_call, null);
//			    	inflatedView.setBackgroundColor(0xB2000000);
			    	
			    	final RelativeLayout loadingView = (RelativeLayout)findViewById(R.id.imgLoadingView);
			    	loadingView.setVisibility(View.VISIBLE);
			    	loadingView.removeAllViews();
			    	
			    	loadingView.addView(inflatedView);
			    	
			    	ImageButton btn_resume = (ImageButton)inflatedView.findViewById(R.id.btn_resume);
			    	btn_resume.setOnTouchListener(new ButtonHighlight(btn_resume));
			    	btn_resume.setOnClickListener(new OnClickListener(){

						@Override
						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							loadingView.removeAllViews();
							loadingView.setVisibility(View.INVISIBLE);
							startGame();
						}});
			    	
			    	ImageButton btn_quit = (ImageButton)inflatedView.findViewById(R.id.btn_quit);
			    	btn_quit.setOnTouchListener(new ButtonHighlight(btn_quit));
			    	
			    	btn_quit.setOnClickListener(new OnClickListener(){

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							stopService(new Intent(MadMathz.this, MediaPlayerService.class));
							MadMathz.this.finish();
							
						}});
				}});
		}
	}
	public boolean bStop = false;
	
	protected void onStop()
	{
		super.onStop();
		
		bStop = true;
		mHandler.removeCallbacks(GetUserInfo);

		if(Preferences.isIncomingCall) //phone call
		{
			stopService(new Intent(this, MediaPlayerService.class));
		} else if(!isContinue) //unexpected down
		{
			stopService(new Intent(this, MediaPlayerService.class));
			Preferences.savePreferences(getApplicationContext());
			this.finish();
		}
		
	}
	
	
	
}
