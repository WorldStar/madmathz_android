package com.facebook.madmathz;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
public class GameView extends Activity  {

private static final String TAG = null;

	// declaretion mediaplayer
	MediaPlayer gVBGM;
	MediaPlayer mSfAlert;
	MediaPlayer mSfWrongRepeat;
	MediaPlayer mSfCorrect;
	//-------------------------
	
	boolean okflg = false;
	boolean repeatflg = false;
	boolean failflg = false;
	TextView txtTargetScore;
    TextView txtScore;
	TextView txtAnswer;
	TextView txtTime;
	private Timer mTimer;
	private TimerTask mTimerTask = null;
	private boolean mRun = false;
	Button btRandom;
	TextView mPoint;
	TextView passNum;
	TextView tvCal;
	TextView tvSum;
	TextView targetScore;
	TextView cTimeView;
	TextView cScoreView;
	TextView UserHelloText;
	TextView UserPointsText;
	ImageView thumbnail;
	Canvas canvas;
	String[] correctNo = new String[11];
	String tmpNo = "";
	int[] button;
	int[] button1;
	int[] button2;
	
	int[] tmpButton = new int[10];
	int[] tmpButton1 = new int[10];
	int[] tmpButton2 = new int[10];
	int cal = 0;
	String calvalue = "";
	
	int[][] effect = (int[][])Array.newInstance(Integer.TYPE, new int[] {3,3});
	int yourPoint;
	Typeface font1;
	Typeface font2;
	
	int gh = 0;
	int gw = 0;
	
	LinearLayout graph;
	
	
	Bitmap[] img;
	Bitmap[] tmp_button;
	Bitmap[] tmp_button1;
	Bitmap[] tmp_button2;
	MyImageView imview;
	
	int mx = 0;
	int mx1 = 0;
	int my = 0;
	int my1 = 0;
	
	int[] no = new int[17];
	int[] selState = new int[17];
	int pass = 0;
	
	int[][] point = (int[][])Array.newInstance(Integer.TYPE, new int[] {3,17});
	
	public int sh = 0;
	public int sw = 0;
	int sum = 0;
	int sumvalue = 0;
	int score = 0;
	boolean moveUpFlg = false;
	
    private long mPassedTime;
    int cTime = 60;
    int tScore;
    
    //save Data
    private SharedPreferences sharedPrefrences;
    private Editor editor;
    
    public Handler mHandler=new Handler()  
    {  
        public void handleMessage(Message msg)  
        {  
            switch(msg.what)  
            {  
	            case 1:  
	            	//cTime--;
	            	cTimeView.setText(Integer.toString(cTime)); 
	            	if(cTime <=10){
	            		cTimeView.setTextColor(Color.RED);
	            	}
	            	cScoreView.setText(Integer.toString(score));
	                break;  
	            case 2:
	            	break;
	            case 3:
	            	if(imview != null)
	            	imview.invalidate();
	            	  
	            	break;
	            default:  
	                break;            
            }  
           // my_layout.invalidate();  
            super.handleMessage(msg);  
        }  
    };  
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	   
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
	    setContentView(R.layout.gameprofile);
	    
	    //---------------------------------
	    if(Preferences.bBGM){
	    	playGameViewBGM();
	    }
	    //---------------------------------
	    
	    //sharedPrefrences = this.getSharedPreferences(Preferences.PREFS_NAME, MODE_WORLD_READABLE);//得到SharedPreferences，会生成user.xml
	    //editor = sharedPrefrences.edit();
	    //yourPoint = sharedPrefrences.getInt("yourPoint", 0);
	    
	    UserHelloText = (TextView)findViewById(R.id.UserHelloText);
	    UserHelloText.setTypeface(Preferences.tf_normal, Typeface.BOLD);
	    UserHelloText.setTextColor(Preferences.tx_color);
	    UserHelloText.setText(Preferences.UserName);
	    
	    UserPointsText = (TextView)findViewById(R.id.UserPointsText);
	    UserPointsText.setTypeface(Preferences.tf_normal);
	    UserPointsText.setTextColor(Preferences.tx_color);
	    
	    txtAnswer = (TextView)findViewById(R.id.txtAnswer);
	    txtScore = (TextView)findViewById(R.id.txtScore);
	    txtTargetScore = (TextView)findViewById(R.id.txtTargetScore);
	    txtTime = (TextView)findViewById(R.id.txtTime);
	    
	    txtAnswer.setTypeface(Preferences.tf_adonais);
	    txtAnswer.setTextColor(Preferences.tx_color);
	    
	    txtScore.setTypeface(Preferences.tf_adonais); 
	    txtScore.setTextColor(Preferences.tx_color);
	    
	    txtTargetScore.setTypeface(Preferences.tf_adonais); 
	    txtTargetScore.setTextColor(Preferences.tx_color);
	    
	    txtTime.setTypeface(Preferences.tf_adonais); 
	    txtTime.setTextColor(Preferences.tx_color);
	    
	    mPoint = (TextView)findViewById(R.id.UserPointsText);
		String strPointer;
		strPointer = "You have " + Preferences.Score + " points";
		mPoint.setText(strPointer);
		mPoint.setTypeface(Preferences.tf_normal); 
		mPoint.setTextColor(Preferences.tx_color);
		
		//thumbnail setting
		thumbnail = (ImageView)findViewById(R.id.thumbnail);
		thumbnail.setTag(String.format(Preferences.FacebookPicUrl, Preferences.FBID));
		mHandler.post(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				new GetPicAsyncTask().execute(thumbnail);
			}
			
		});
		
	    Random rand = new Random();
	    this.tmpButton = new int[]{-1,
	    		R.drawable.button1,R.drawable.button2,R.drawable.button3,
	    		R.drawable.button4,R.drawable.button5,R.drawable.button6,
	    		R.drawable.button7,R.drawable.button8,R.drawable.button9
	    };
	    this.tmpButton1 = new int[]{-1,
	    		R.drawable.button11,R.drawable.button21,R.drawable.button31,
	    		R.drawable.button41,R.drawable.button51,R.drawable.button61,
	    		R.drawable.button71,R.drawable.button81,R.drawable.button91
	    };
	    this.tmpButton2 = new int[]{-1,
	    		R.drawable.button12,R.drawable.button22,R.drawable.button32,
	    		R.drawable.button42,R.drawable.button52,R.drawable.button62,
	    		R.drawable.button72,R.drawable.button82,R.drawable.button92
	    };
	    this.selState = new int[] {-1,
	    		0,0,0,0,
	    		0,0,0,0,
	    		0,0,0,0,
	    		0,0,0,0		
	    };
	    this.correctNo = new String[]{
	    		"-1","0","0","0","0","0","0","0","0","0","0"
	    };
	 
	    this.no = new int[] {-1,
	    		Preferences.cur_ChallengeList.Tiles[0],
	    		Preferences.cur_ChallengeList.Tiles[1],
	    		Preferences.cur_ChallengeList.Tiles[2],
	    		Preferences.cur_ChallengeList.Tiles[3],
	    		Preferences.cur_ChallengeList.Tiles[4],
	    		Preferences.cur_ChallengeList.Tiles[5],
	    		Preferences.cur_ChallengeList.Tiles[6],
	    		Preferences.cur_ChallengeList.Tiles[7],
	    		Preferences.cur_ChallengeList.Tiles[8],
	    		Preferences.cur_ChallengeList.Tiles[9],
	    		Preferences.cur_ChallengeList.Tiles[10],
	    		Preferences.cur_ChallengeList.Tiles[11],
	    		Preferences.cur_ChallengeList.Tiles[12],
	    		Preferences.cur_ChallengeList.Tiles[13],
	    		Preferences.cur_ChallengeList.Tiles[14],
	    		Preferences.cur_ChallengeList.Tiles[15],
	    };
	    this.button = new int[] { -1,
	    		tmpButton[no[1]], tmpButton[no[2]], tmpButton[no[3]], tmpButton[no[4]],
	    		tmpButton[no[5]], tmpButton[no[6]], tmpButton[no[7]], tmpButton[no[8]],
	    		tmpButton[no[9]], tmpButton[no[10]], tmpButton[no[11]], tmpButton[no[12]],
	    		tmpButton[no[13]], tmpButton[no[14]], tmpButton[no[15]], tmpButton[no[16]]
	    };
	    this.button1 = new int[] { -1,
	    		tmpButton1[no[1]], tmpButton1[no[2]], tmpButton1[no[3]], tmpButton1[no[4]],
	    		tmpButton1[no[5]], tmpButton1[no[6]], tmpButton1[no[7]], tmpButton1[no[8]],
	    		tmpButton1[no[9]], tmpButton1[no[10]], tmpButton1[no[11]], tmpButton1[no[12]],
	    		tmpButton1[no[13]], tmpButton1[no[14]], tmpButton1[no[15]], tmpButton1[no[16]]
	    };
	    this.button2 = new int[] { -1,
	    		tmpButton2[no[1]], tmpButton2[no[2]], tmpButton2[no[3]], tmpButton2[no[4]],
	    		tmpButton2[no[5]], tmpButton2[no[6]], tmpButton2[no[7]], tmpButton2[no[8]],
	    		tmpButton2[no[9]], tmpButton2[no[10]], tmpButton2[no[11]], tmpButton2[no[12]],
	    		tmpButton2[no[13]], tmpButton2[no[14]], tmpButton2[no[15]], tmpButton2[no[16]]
	    };
	    this.img = new Bitmap[17];
	    this.tmp_button = new Bitmap[17];	    
	    this.tmp_button1 = new Bitmap[17];
	    this.tmp_button2 = new Bitmap[17];
	    
	    Display localDisplay = getWindowManager().getDefaultDisplay();
	    this.sw = localDisplay.getWidth();
	    this.sh = localDisplay.getHeight();
	    
	    this.graph = (LinearLayout) findViewById(R.id.graphLayout);	   
	
	    this.sum = 0;
	    
	    this.canvas = new Canvas();
	    
	   	this.imview = new MyImageView(this);
	    this.graph.addView(this.imview);
	    
	    this.passNum = (TextView) findViewById(R.id.passNum);
	    this.targetScore = (TextView) findViewById(R.id.targetscore);
	    this.cTimeView = (TextView) findViewById(R.id.ctime);
	    this.cScoreView= (TextView) findViewById(R.id.score);
	    cTimeView.setTypeface(Preferences.tf_adonais);
	    cTimeView.setTextColor(Preferences.tx_color);
	    passNum.setTypeface(Preferences.tf_adonais); 
	    passNum.setTextColor(Preferences.tx_color);
	    targetScore.setTypeface(Preferences.tf_adonais); 
	    targetScore.setTextColor(Preferences.tx_color);
	    
	    cScoreView.setTypeface(Preferences.tf_adonais);
	    cScoreView.setTextColor(Preferences.tx_color);
	    	    
	    for (int i=0; i<3; i++)
	    	for (int n=0; n<3; n++)
	    		this.effect[i][n] = 0;	

	    //pass = 15 + rand.nextInt(15);
	    pass = Preferences.cur_ChallengeList.answer;
		passNum.setText("" + pass);

		tScore = Preferences.cur_ChallengeList.possibility;
		targetScore.setText(Integer.toString(tScore));
				
		cTimeView.setText(Integer.toString(cTime));
		cScoreView.setText(Integer.toString(score));
		
		boardbg = (RelativeLayout)findViewById(R.id.boardbg);
		
		img_result = new ImageView(this);
//		img_result.setImageResource(R.drawable.ok);
		
		star1 = new ImageView(this);
		star2 = new ImageView(this);
		star3 = new ImageView(this);
		star4 = new ImageView(this);
		star5 = new ImageView(this);
		star6 = new ImageView(this);
		
		star1.setImageResource(R.drawable.star);
		star2.setImageResource(R.drawable.star);
		star3.setImageResource(R.drawable.star);
		star4.setImageResource(R.drawable.star);
		star5.setImageResource(R.drawable.star);
		star6.setImageResource(R.drawable.star);
		
		boardbg.addView(img_result);
		boardbg.addView(star1);
		boardbg.addView(star2);
		boardbg.addView(star3);
		boardbg.addView(star4);
		boardbg.addView(star5);
		boardbg.addView(star6);
		
		img_result.setVisibility(View.INVISIBLE);
		star1.setVisibility(View.INVISIBLE);
		star2.setVisibility(View.INVISIBLE);
		star3.setVisibility(View.INVISIBLE);
		star4.setVisibility(View.INVISIBLE);
		star5.setVisibility(View.INVISIBLE);
		star6.setVisibility(View.INVISIBLE);
		
		
		//image init
		
		
		
		thread=new GameThread();  
        thread.start();  
	}
	GameThread thread;
	RelativeLayout boardbg;
	
	ImageView star1;
	ImageView star2;
	ImageView star3;
	ImageView star4;
	ImageView star5;
	ImageView star6;
	
	
	public void playGameViewBGM(){
		if(!Preferences.bBGM) return;

		gVBGM = MediaPlayer.create(getApplicationContext(), R.raw.gamecountdown);
       
        gVBGM.start();
	}
	
	public void stopGameViewBGM(){
		
		if (gVBGM != null){       	 
			gVBGM.stop();
			gVBGM.release();
			gVBGM = null;
        }
		
	}
	public void sfCorrect(){
		if(!Preferences.bSound) return;
		mSfCorrect = MediaPlayer.create(getApplicationContext(), R.raw.correct);
	       
		mSfCorrect.start();
	}
	public void sfWrong_Repeat(){
		if(!Preferences.bSound) return;
		mSfWrongRepeat = MediaPlayer.create(getApplicationContext(), R.raw.wrongrepeat);
	       
		mSfWrongRepeat.start();
	}
	
	public void sfAlert(){
		if(!Preferences.bSound) return;
		mSfAlert = MediaPlayer.create(getApplicationContext(), R.raw.alert);
	       
		mSfAlert.start();
	}
	
	
	private class GameThread extends Thread
	{

		@Override
		public void run() {
			// TODO Auto-generated method stub
			        Log.e("ok", "111111111");  
			        
			        // TODO Auto-generated method stub  
			        flag = true;
			        
			        while(flag)
			        {
			        	try {
							Thread.sleep(1000);
							if(bStopThread) continue;
							
							doCountTime();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			        	
			        }
			        
			        
			        //Message message=new Message();  
			        //message.what=1;  	        
					//mHandler.sendMessage(message);  
			       
		}
		
	}
	 
	public void doCountTime(){
		//this.graph.invalidate();
		cTime--;
		if(cTime >=0 && cTime <= 10){
			sfAlert();
		}
		if(repeatflg || failflg){
			sfWrong_Repeat();
			repeatflg = false;
			failflg = false;
		}
		if(okflg){
			sfCorrect();
			okflg = false;
		}
		if(cTime >=0 && score < tScore){
			Message message=new Message();
			message.what=1;  	        
			mHandler.sendMessage(message); 
		}
		if(cTime >= 0 && score == tScore){
			//-------------------------
			stopGameViewBGM();
			//-------------------------
			if(Preferences.cur_ChallengeList.fplayer_id.equals(Preferences.FBID))
			{
				Preferences.cur_ChallengeList.fplayer_comp_time = 60 - cTime;
				Preferences.cur_ChallengeList.fplayer_comp_poss = score;
			}
			else
			{
				Preferences.cur_ChallengeList.splayer_comp_time = 60 - cTime;
				Preferences.cur_ChallengeList.splayer_comp_poss = score;
			}
			
//			if(Preferences.cur_ChallengeList.isFPlayer) Preferences.cur_ChallengeList.fplayer_comp_time = cTime;
//			else Preferences.cur_ChallengeList.splayer_comp_time = cTime;
			
			cTime = -1;
			isContinue = true;
			GameView.this.finish();
			Intent intent = new Intent(GameView.this, StartInGame.class);		 
			intent.putExtra("GameState", StartInGame.STATE_END);
   			intent.putExtra("setImgView" , "startGameWin");
   		    startActivity(intent);	
		}
		if(cTime == -1 && score < tScore){
			//------------------------------
			stopGameViewBGM();
			//------------------------------
			if(Preferences.cur_ChallengeList.fplayer_id.equals(Preferences.FBID))
			{
				Preferences.cur_ChallengeList.fplayer_comp_time = 60;
				Preferences.cur_ChallengeList.fplayer_comp_poss = score;
			}else
			{
				Preferences.cur_ChallengeList.splayer_comp_time = 60;
				Preferences.cur_ChallengeList.splayer_comp_poss = score;
			}
//			if(Preferences.cur_ChallengeList.isFPlayer) Preferences.cur_ChallengeList.fplayer_comp_time = 60;
//			else Preferences.cur_ChallengeList.splayer_comp_time = 60;
			
			//GameView.this.finish();
			Intent intent = new Intent(GameView.this, StartInGame.class);		 
			intent.putExtra("GameState", StartInGame.STATE_END);
   			intent.putExtra("setImgView" , "startGameLose");
   		    startActivity(intent);	
   		    isContinue = true;
   		    GameView.this.finish();
		}
  	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
	}

	public void onClick(View v) {
		//Random rand = new Random();
		//pass = 20 + rand.nextInt(54);
		//passNum.setText("" + pass);
	}

	public class MyImageView extends View {

		int beth = 0;
	    int betw = 0;
	    int imgh = 0;
	    int imgw = 0;
	    
	   
	    
	    int oldi = 0;
	    int selectRes = 0;
	    int[] tmp;
	    int x = 0;
	    int y = 0;
	    int lw,lh;
	    Paint effectPaint;
	    
	    Bitmap bmp_happy;
	    Bitmap bmp_shine;
	    Bitmap bmp_ok;
	    Bitmap bmp_fail;
	    Bitmap bmp_repeat;
	    int h = 0;
	    
	    int gap_w = 8;
	    
		public MyImageView(Context context) {
			super(context);
			Display localDisplay = getWindowManager().getDefaultDisplay();
			lw = localDisplay.getWidth();
			this.tmp = new int[17];
			//gw = graph.getWidth();
			gw = localDisplay.getWidth();
			gh = graph.getHeight();
			
			betw = gw  / (7 + 8*4);
			imgw = betw * 8;
			imgh = betw * 8 * 70 / 66;
			
			for (int i=1; i<17; i++) {
				this.tmp[i] = 0;
				img[i] = BitmapFactory.decodeResource(getResources(), button[i]);
				//mBackgroundImageFar = Bitmap.createScaledBitmap(mBackgroundImageFar,
                //        mCanvasWidth * 2, mCanvasHeight, true);
				//if(lw == 320){
					img[i] = Bitmap.createScaledBitmap(img[i],
		                        imgw, imgh, true);
				//}
				tmp_button[i] = BitmapFactory.decodeResource(getResources(), button[i]);
				tmp_button[i] = Bitmap.createScaledBitmap(tmp_button[i], imgw, imgh, true);

				tmp_button1[i] = BitmapFactory.decodeResource(getResources(), button1[i]);
				tmp_button1[i] = Bitmap.createScaledBitmap(tmp_button1[i], imgw, imgh, true);

				tmp_button2[i] = BitmapFactory.decodeResource(getResources(), button2[i]);
				tmp_button2[i] = Bitmap.createScaledBitmap(tmp_button2[i], imgw, imgh, true);

			}
		
			effectPaint = new Paint();
			effectPaint.setColor(Color.GREEN);
			effectPaint.setStyle(Paint.Style.FILL_AND_STROKE);
			effectPaint.setAlpha(35);
			
			bmp_happy = BitmapFactory.decodeResource(getResources(), R.drawable.happy);
			bmp_shine = BitmapFactory.decodeResource(getResources(), R.drawable.star_zoom);
			//bmp_shine = Bitmap.createScaledBitmap(bmp_shine,gw, gh, true); 
					
			bmp_ok = BitmapFactory .decodeResource(getResources(), R.drawable.ok);
			bmp_fail = BitmapFactory.decodeResource(getResources(), R.drawable.fail);
			bmp_repeat = BitmapFactory.decodeResource(getResources(), R.drawable.repeat);
		}
		
		private int delta_w = 0;
		
		@Override
		protected void onDraw(Canvas canvas) {
			if(moveUpFlg){
				//img[selectRes] = BitmapFactory.decodeResource(getResources(), button2[selectRes]);
				img[selectRes] = tmp_button2[selectRes]; //BitmapFactory.decodeResource(getResources(), button2[selectRes]);
			}
			
			for (int k=1; k<=16; k++) {

				if (k==1) {
					gw = graph.getWidth();
					gh = graph.getHeight();

					delta_w = img[k].getWidth() / 10;
					
					//imgw = img[k].getWidth();
					//betw = (gw - imgw*4) / 7;
					//imgh = img[k].getHeight();
					beth = betw/2;
				}
				
				int c = (k-1) % 4;
				int r = (k-1) / 4;
				
				//if(lw != 320){
					h = (gh - 4*imgh - 3*beth)/2;
				//}else if (lw == 320){
				//	h = 0; 
				//}

				//org_width
				point[1][k] = betw + c * imgw + (c+1) * betw;
				
				//org_height
				point[2][k] = h + r * imgh + (r+1) * beth;
				
				if (pass >= 15) {
					if (point[1][k] + delta_w < mx && point[1][k] + imgw - delta_w * 2 > mx && point[2][k] + delta_w < my && point[2][k] + imgh - delta_w * 2> my) {
						
							//img[k] = BitmapFactory.decodeResource(getResources(), button1[k]);
							//img[k] = Bitmap.createScaledBitmap(img[k], imgw, imgh, true);
							img[k] = tmp_button1[k];
						
							boolean is_clicked = false;
							selectRes = k;
							selState[k]=1;
							
							for(int i = 0; i < clicked_tiles.size(); i++)
							{
								if(clicked_tiles.get(i) == k) 
								{
									is_clicked = true;
									break;
								}
							}
							
							if(!is_clicked)
							{
								clicked_tiles.add(k);
							}
						//if (this.oldi != k) {
							if(!is_clicked){
								
							if (sumvalue <= pass) {
								
								sumvalue = sumvalue + no[k];
								//tvSum.setText(" = " + sumvalue);
								
								if (this.tmp[k] != 0)
									sumvalue = 101;

								tmp[k] = no[k];
								positionvalue += Math.pow(k, 3);
								//tmpNo += String.valueOf(no[k]);
								
							}
						}
						//this.oldi = k;
					}
				}
				if(!moveUpFlg){
					//img[k] = Bitmap.createScaledBitmap(img[k], imgw, imgh, true);
							
					canvas.drawBitmap(img[k], point[1][k], point[2][k], null);
					
				}else{
					if(selState[k] == 1){
						//img[k] = BitmapFactory.decodeResource(getResources(), button2[k]);
						//img[k] = Bitmap.createScaledBitmap(img[k], imgw, imgh, true);
						img[k] = tmp_button2[k];
//						}
						selState[k] = 0;
					}else{
						//img[k] = BitmapFactory.decodeResource(getResources(), button[k]);
						//img[k] = Bitmap.createScaledBitmap(img[k], imgw, imgh, true);
						img[k] = tmp_button[k];
						
					}
					canvas.drawBitmap(img[k], point[1][k], point[2][k], null);
				}
				
				
			}
			
			//canvas.drawRect(effect[1][1], effect[2][1], effect[1][2], effect[2][2], effectPaint);
			
			if (moveUpFlg) {
				if( pass == sumvalue) {
					
					for(int i = 0; i < repeat_list.size(); i++)
					{
						if(repeat_list.get(i) == positionvalue)
						{
					//for(int i=1;i<correctNo.length;i++){
					//	if(correctNo[i].equals(tmpNo)){
							positionvalue = 0;
							
							repeatflg = true;
							if(Preferences.FBID.equals(Preferences.cur_ChallengeList.fplayer_id))
								Preferences.cur_ChallengeList.fplayer_repeat++;
							else Preferences.cur_ChallengeList.splayer_repeat++;
							tmpNo = "";
							
							//current_effect = EFFECT_REPEAT;
							mHandler.post(new Runnable(){

								@Override
								public void run() {
									// TODO Auto-generated method stub
									resultbg(EFFECT_REPEAT);
								}});
//							canvas.drawBitmap(bmp_repeat, gw / 2 - bmp_repeat.getWidth() / 2, (gh-bmp_repeat.getHeight())/2, null);
						
						}
					}
					if(repeatflg == false){
						score++;
						if(Preferences.FBID.equals(Preferences.cur_ChallengeList.fplayer_id))
								Preferences.cur_ChallengeList.fplayer_score++;
						else Preferences.cur_ChallengeList.splayer_score++;
						
						repeat_list.add(positionvalue);
						//correctNo[score] = tmpNo;
						positionvalue = 0;
						//tmpNo = "";
						okflg = true;
						//sfCorrect();
						//current_effect= EFFECT_SHINE;
						//effect_count = 0;
						
						mHandler.post(new Runnable(){

							@Override
							public void run() {
								// TODO Auto-generated method stub
								transitionstar();
								resultbg(EFFECT_SHINE);
							}});
						
						/*
						if(lw == 320){
						canvas.drawBitmap(bmp_shine, 0, 0, null);
						}else{
							canvas.drawBitmap(bmp_shine, betw, h, null);
						}
						canvas.drawBitmap(bmp_ok, gw / 2 - bmp_ok.getWidth() / 2, (gh-bmp_ok.getHeight())/2, null);
						*/
						
					}
				} else if (pass != sumvalue) {
					positionvalue = 0;
					failflg = true;
					if(Preferences.FBID.equals(Preferences.cur_ChallengeList.fplayer_id))
							Preferences.cur_ChallengeList.fplayer_wrong_tried++;
					else Preferences.cur_ChallengeList.splayer_wrong_tried++;
					
					if (!okflg)  
					{
						//current_effect = EFFECT_FAIL;
						//effect_count = 0;
						
						mHandler.post(new Runnable(){

							@Override
							public void run() {
								// TODO Auto-generated method stub
								resultbg(EFFECT_FAIL);
							}});
						
					}
						//canvas.drawBitmap(bmp_fail, gw / 2 - bmp_fail.getWidth() / 2, (gh-bmp_fail.getHeight())/2, null);
					
				}
				/*
				
				//draw effect img
				if(current_effect != 0)
				{
					effect_count++;
					if(effect_count >= MAX_COUNT)
					{
						effect_count = 0;
						current_effect = EFFECT_NO;
					}
					
					switch(current_effect)
					{
					case EFFECT_FAIL:
						canvas.drawBitmap(bmp_fail, gw / 2 - bmp_fail.getWidth() / 2, (gh-bmp_fail.getHeight())/2, null);
						break;
					case EFFECT_REPEAT:
						canvas.drawBitmap(bmp_repeat, gw / 2 - bmp_repeat.getWidth() / 2, (gh-bmp_repeat.getHeight())/2, null);
						
						break;
					case EFFECT_SHINE:
						if(lw == 320){
						canvas.drawBitmap(bmp_shine, 0, 0, null);
						}else{
							canvas.drawBitmap(bmp_shine, betw, h, null);
						}
						canvas.drawBitmap(bmp_ok, gw / 2 - bmp_ok.getWidth() / 2, (gh-bmp_ok.getHeight())/2, null);

						break;
					
					}
				}
				*/
				mx = 0;
				my = 0;
				moveUpFlg = false;
				for (int i=1; i<17; i++) {
					this.tmp[i] = 0;
					//img[i] = BitmapFactory.decodeResource(getResources(), button[i]);
					//img[i] = Bitmap.createScaledBitmap(img[i], imgw, imgh, true);
					img[i] = tmp_button[i];
				}
				invalidate();
			}	 
			
	      //  canvas.drawBitmap(bmp_happy, gw / 2 - bmp_happy.getWidth() / 2, gh - bmp_happy.getHeight(), null);
		}
		
	
		
		public ArrayList<Integer> repeat_list = new ArrayList<Integer>();
		public ArrayList<Integer> clicked_tiles = new ArrayList<Integer>();
		public int positionvalue = 0;
		
		
		
		@Override
		public boolean onTouchEvent(MotionEvent event) {
			
			if(Preferences.isIncomingCall) return false;

			switch (event.getAction() ) {
			
			case MotionEvent.ACTION_DOWN:
				sumvalue = 0;
				calvalue = "";
				cal = 0;
				oldi = 0;
				clicked_tiles.clear();
				positionvalue = 0;
			

				okflg = false;
				repeatflg = false;
				failflg = false;
				for (int i=1; i<17; i++) tmp[i] = 0;
				
			case MotionEvent.ACTION_MOVE:
				mx = (int) event.getX();
				my = (int) event.getY();
				
				effect[1][1] = mx1 - 45;
				effect[1][2] = mx + 45;
				effect[2][1] = my1 - 45;
				effect[2][2] = my + 45;
					
				mx1 = mx;
				my1 = my;
				
				//invalidate();
				Message msg = new Message();
				msg.what = 3;
				mHandler.sendMessage(msg);
				break;

			case MotionEvent.ACTION_UP:
				moveUpFlg = true;
				img[selectRes] = BitmapFactory.decodeResource(getResources(), button2[selectRes]);
				invalidate();
				break;
				
			default:
				break;
			}
			
			return true;
		}
		
	}
	public int current_effect = 0;
	public int effect_count = 0;
	public static final int EFFECT_NO = 0;
	public static final int MAX_COUNT = 100;
	public static final int EFFECT_FAIL = 1;
	public static final int EFFECT_REPEAT = 2;
	public static final int EFFECT_SHINE = 3;
	
	ImageView img_result;
	
	
	
	public void transitionstar()
	{
		star1.setVisibility(View.VISIBLE);
		star2.setVisibility(View.VISIBLE);
		star3.setVisibility(View.VISIBLE);
		star4.setVisibility(View.VISIBLE);
		star5.setVisibility(View.VISIBLE);
		star6.setVisibility(View.VISIBLE);

		int x = (gw - star1.getWidth()) / 2;
		int y = (gh - star1.getHeight()) / 2;

		Animation scale = new ScaleAnimation(1, 1, 1, 1, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
		// 1 second duration
		scale.setDuration(300);
		
		RotateAnimation rotate = new RotateAnimation(0, -90, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		rotate.setDuration(300);
		RotateAnimation rotate1 = new RotateAnimation(0, 90, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		rotate1.setDuration(300);
		RotateAnimation rotate2 = new RotateAnimation(0, 90, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		rotate2.setDuration(300);
		RotateAnimation rotate3 = new RotateAnimation(90, -90, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		rotate3.setDuration(300);
		RotateAnimation rotate4 = new RotateAnimation(90, 0, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		rotate4.setDuration(300);
		RotateAnimation rotate5 = new RotateAnimation(0, 90, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		rotate5.setDuration(300);
		
		// Moving up
		Animation tralslate = new TranslateAnimation(x - 20,-800, y -15, -800);	
		tralslate.setDuration(500);
		Animation tralslate1 = new TranslateAnimation(x - 10,x +  600, y + 3, -600);	
		tralslate1.setDuration(500);
		Animation tralslate2 = new TranslateAnimation(x + 13, x+ 400,  y + 5,y + 400);	
		tralslate2.setDuration(500);
		Animation tralslate3 = new TranslateAnimation(x - 3,  -200, y + 18,y +  200);	
		tralslate3.setDuration(500);
		Animation tralslate4 = new TranslateAnimation( x + 20, x + 900,  y - 15,  -300);	
		tralslate4.setDuration(500);
		Animation tralslate5 = new TranslateAnimation( x,x -200,y + 3, y + 500);	
		tralslate5.setDuration(500);
	
		AnimationSet animSet = new AnimationSet(true);
		//animSet.setFillEnabled(true);
		animSet.setFillEnabled(true);
		animSet.setFillAfter(true);
		animSet.addAnimation(scale);
		animSet.addAnimation(rotate);
		animSet.addAnimation(tralslate);
		animSet.setDuration(500);
		star6.startAnimation(animSet);
		
		AnimationSet animSet1 = new AnimationSet(true);
		//animSet1.setFillEnabled(true);
		animSet1.setFillEnabled(true);
		animSet1.setFillAfter(true);
		animSet1.addAnimation(scale);
		animSet1.addAnimation(rotate1);
		animSet1.addAnimation(tralslate1);
		animSet1.setDuration(500);
		star1.startAnimation(animSet1);
		
		AnimationSet animSet2 = new AnimationSet(true);
		//animSet2.setFillEnabled(true);
		animSet2.setFillEnabled(true);
		animSet2.setFillAfter(true);
		animSet2.addAnimation(scale);
		animSet2.addAnimation(rotate2);
		animSet2.addAnimation(tralslate2);
		animSet2.setDuration(500);
		star2.startAnimation(animSet2);
		
		AnimationSet animSet3 = new AnimationSet(true);
		//animSet3.setFillEnabled(true);
		animSet3.setFillEnabled(true);
		animSet3.setFillAfter(true);
		animSet3.addAnimation(scale);
		animSet3.addAnimation(rotate3);
		animSet3.addAnimation(tralslate3);
		animSet3.setDuration(500);
		star3.startAnimation(animSet3);
		
		AnimationSet animSet4 = new AnimationSet(true);
		//animSet4.setFillEnabled(true);
		animSet4.setFillEnabled(true);
		animSet4.setFillAfter(true);
		animSet4.addAnimation(scale);
		animSet4.addAnimation(rotate4);
		animSet4.addAnimation(tralslate4);
		animSet4.setDuration(500);
		star4.startAnimation(animSet4);
	
		AnimationSet animSet5 = new AnimationSet(true);
		//animSet5.setFillEnabled(true);
		animSet5.setFillEnabled(true);
		animSet5.setFillAfter(true);
		animSet5.addAnimation(scale);
		animSet5.addAnimation(rotate5);
		animSet5.addAnimation(tralslate5);
		animSet5.setDuration(500);
		star5.startAnimation(animSet5);
		
		animSet.setAnimationListener(new AnimationListener(){

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				star1.setVisibility(View.INVISIBLE);
				star2.setVisibility(View.INVISIBLE);
				star3.setVisibility(View.INVISIBLE);
				star4.setVisibility(View.INVISIBLE);
				star5.setVisibility(View.INVISIBLE);
				star6.setVisibility(View.INVISIBLE);
				
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				
			}});
	
	}
	
	public void resultbg(int state)
	{
		img_result.setVisibility(View.VISIBLE);
		Bitmap 	img_tmp = BitmapFactory.decodeResource(getResources(), R.drawable.fail);

		switch(state)
		{
			case EFFECT_FAIL:
//				img_result.setBackgroundResource(R.drawable.fail);
				img_tmp = BitmapFactory.decodeResource(getResources(), R.drawable.fail);
				break;
			case EFFECT_REPEAT:
//				img_result.setBackgroundResource(R.drawable.repeat);
				img_tmp = BitmapFactory.decodeResource(getResources(), R.drawable.repeat);
				//img_result.setImageResource(R.drawable.repeat);
				break;
			case EFFECT_SHINE:
//				img_result.setBackgroundResource(R.drawable.ok);
				img_tmp = BitmapFactory.decodeResource(getResources(), R.drawable.ok);
				//img_result.setImageResource(R.drawable.ok);
				break;
		}
		
		img_result.setImageBitmap(img_tmp);
//		int img_x = (gw - img_result.getWidth()) / 2;
//		int img_y = (gh - img_result.getHeight()) / 2;
		int img_x = (gw - img_tmp.getWidth()) / 2;
		int img_y = (gh - img_tmp.getHeight()) / 2;
		Animation anim = new AlphaAnimation(0.0f, 1.0f);
		anim.setDuration(300);
		
		Animation tralslate = new TranslateAnimation(img_x, img_x, img_y, img_y);	
		tralslate.setDuration(300);
		
		AnimationSet animSet1 = new AnimationSet(true);
		animSet1.addAnimation(anim);
		animSet1.addAnimation(tralslate);

		
		final Animation anim1 = new AlphaAnimation(1.0f, 1.0f);
		anim1.setDuration(100);
		Animation translate = new TranslateAnimation(img_x, img_x, img_y, img_y);
		translate.setDuration(100);
		
		final AnimationSet animSet2 = new AnimationSet(true);
		animSet2.addAnimation(anim1);
		animSet2.addAnimation(translate);
		
		img_result.startAnimation(animSet1);
		
		animSet1.setAnimationListener(new AnimationListener(){

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				img_result.startAnimation(animSet2);
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				
			}});
		
		anim1.setAnimationListener(new AnimationListener(){

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				img_result.setVisibility(View.INVISIBLE);

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				
			}});
	}
	
	public boolean isContinue = false;
	
	protected void onResume()
	{
		super.onResume();

		if(Preferences.isIncomingCall)
		{
			mHandler.post(new Runnable(){ //call process

				@Override
				public void run() {
					// TODO Auto-generated method stub
					
			    	LayoutInflater mInflater = (LayoutInflater) GameView.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			    	View inflatedView = mInflater.inflate(R.layout.phone_call, null);
			    	
			    	final RelativeLayout loadingView = (RelativeLayout)findViewById(R.id.loadingView);
			    	loadingView.setVisibility(View.VISIBLE);
			    	loadingView.setBackgroundColor(0xB2000000);
			    	loadingView.removeAllViews();
			    	
			    	loadingView.addView(inflatedView);
			    	
			    	ImageButton btn_resume = (ImageButton)inflatedView.findViewById(R.id.btn_resume);
			    	btn_resume.setOnTouchListener(new ButtonHighlight(btn_resume));
			    	btn_resume.setOnClickListener(new OnClickListener(){

						@Override
						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							loadingView.setVisibility(View.INVISIBLE);
							loadingView.removeAllViews();
//							thread.resume();
							Preferences.isIncomingCall = false;
							bStopThread = false;
							if(Preferences.bBGM)playGameViewBGM();
						}});
			    	
			    	ImageButton btn_quit = (ImageButton)inflatedView.findViewById(R.id.btn_quit);
			    	btn_quit.setOnTouchListener(new ButtonHighlight(btn_quit));
			    	
			    	btn_quit.setOnClickListener(new OnClickListener(){

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							stopGameViewBGM();
							Preferences.isIncomingCall = false;
							GameView.this.finish();
							
						}});
				}});
		}
		
	}
	
	public boolean bStopThread = false;
	public boolean flag;
	protected void onStop()
	{
		super.onStop();
		if(Preferences.isIncomingCall)
		{
			stopGameViewBGM();
//			thread.suspend();
			bStopThread = true;
		}else if(!isContinue) //close app by accidently
		{
			stopGameViewBGM();
//			thread.stop();
			flag = false; // this will force secondary to finish its execution
			try {
			    thread.join(); // wait for secondary to finish
			} catch (InterruptedException e) {
			    throw new RuntimeException(e);
			}			
			this.finish();
		}
		
		
	}
}
