package com.facebook.madmathz;

public class ChallengeList
{
	public String user;
	public String id;
	public String fplayer_id;
	public String splayer_id;
	public String fplayer_msg;
	public String splayer_msg;
	public int fplayer_comp_time;
	public int splayer_comp_time;
	public int fplayer_comp_poss;
	public int splayer_comp_poss;
	public String match_played_time;
	public int fplayer_wrong_tried;
	public int splayer_wrong_tried;
	public String gameplay_id;
	public String match_id;
	public int status;
	public String fplayer_username;
	public String splayer_username;
	public int fplayer_repeat;
	public int splayer_repeat;
	public int result;
	public int DiffTime;

	public int Tiles[];
	public int possibility;
	public int fplayer_score;
	public int splayer_score;
	public String WinnerID;
	public int answer;
	public boolean isFPlayer;
	//
	public int GameType;
	public int GameResult;
}