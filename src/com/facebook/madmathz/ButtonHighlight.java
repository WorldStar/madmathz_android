package com.facebook.madmathz;

import android.graphics.Color;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;

public class ButtonHighlight implements OnTouchListener {

	  final ImageButton imageButton;

	  public ButtonHighlight(final ImageButton imageButton) {
	    super();
	    this.imageButton = imageButton;
	  }

	  public boolean onTouch(final View view, final MotionEvent motionEvent) {
	    if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
	      //grey color filter, you can change the color as you like
	      imageButton.setColorFilter(Color.argb(155, 85, 85, 85));
	    } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
	      imageButton.setColorFilter(Color.argb(0, 85, 85, 85)); 
	    }
	    return false;
	  }

	}