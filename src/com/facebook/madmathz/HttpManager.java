package com.facebook.madmathz;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.GsonBuilder;

import android.util.Log;

public class HttpManager {
	
	public static String APP_TAG = "MadMathz";
	
	public static String getHttpResponse(URI uri) {
		Log.d(APP_TAG, "Going to make a get request");
		System.out.println("[url ] " + uri.toString());
		StringBuilder response = new StringBuilder();
		try {
			HttpGet get = new HttpGet();
			get.setURI(uri);
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpResponse httpResponse = httpClient.execute(get);
			if (httpResponse.getStatusLine().getStatusCode() == 200) {
				Log.d("demo", "HTTP Get succeeded");

				HttpEntity messageEntity = httpResponse.getEntity();
				InputStream is = messageEntity.getContent();
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				String line;
				while ((line = br.readLine()) != null) {
					response.append(line);
				}
			}
		} catch (Exception e) {
			Log.e("demo", e.getMessage());
		}
		Log.d("demo", "Done with HTTP getting");
		
		
		System.out.println("[response] " + response.toString());
		return response.toString();
	}
	
	private static JSONObject getJsonObjectFromMap(Map params) throws JSONException {

	    //all the passed parameters from the post request
	    //iterator used to loop through all the parameters
	    //passed in the post request
	    Iterator iter = params.entrySet().iterator();

	    //Stores JSON
	    JSONObject holder = new JSONObject();

	    //using the earlier example your first entry would get email
	    //and the inner while would get the value which would be 'foo@bar.com' 
	    //{ fan: { email : 'foo@bar.com' } }

	    //While there is another entry
	    while (iter.hasNext()) 
	    {
	        //gets an entry in the params
	        Map.Entry pairs = (Map.Entry)iter.next();

	        //creates a key for Map
	        String key = (String)pairs.getKey();

	        //Create a new map
	        Map m = (Map)pairs.getValue();   

	        //object for storing Json
	        JSONObject data = new JSONObject();

	        //gets the value
	        Iterator iter2 = m.entrySet().iterator();
	        while (iter2.hasNext()) 
	        {
	            Map.Entry pairs2 = (Map.Entry)iter2.next();
	            data.put((String)pairs2.getKey(), (String)pairs2.getValue());
	        }

	        //puts email and 'foo@bar.com'  together in map
	        holder.put(key, data);
	    }
	    return holder;
	}
	
	public static String postHttpResponse(URI uri, HashMap params) {
		Log.d(APP_TAG, "Going to make a post request");
		StringBuilder response = new StringBuilder();
		try {
			HttpPost post = new HttpPost();
			post.setURI(uri);
			System.out.println("[url ] " + uri.toASCIIString());
			//List params = new ArrayList();
			//params.add(new BasicNameValuePair("paramName", "paramValue"));
			String json = new GsonBuilder().create().toJson(params, HashMap.class);
			System.out.println("[param ]"  + json.toString());
			//JSONObject holder = getJsonObjectFromMap(params);
			StringEntity se = new StringEntity(json);
			post.setEntity(se);
		    post.setHeader("Accept", "application/json");
		    post.setHeader("Content-type", "application/json");
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpResponse httpResponse = httpClient.execute(post);
			if (httpResponse.getStatusLine().getStatusCode() == 200) {
				Log.d(APP_TAG, "HTTP POST succeeded");
				HttpEntity messageEntity = httpResponse.getEntity();
				InputStream is = messageEntity.getContent();
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				String line;
				while ((line = br.readLine()) != null) {
					response.append(line);
				}
			} else {
				Log.e(APP_TAG, "HTTP POST status code is not 200");
			}
		} catch (Exception e) {
			Log.e(APP_TAG, e.getMessage());
		}
		Log.d(APP_TAG, "Done with HTTP posting");
		System.out.println("[response ] " + response.toString());
		return response.toString();
	}
}
