package com.facebook.madmathz;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.facebook.*;
import com.facebook.android.AsyncFacebookRunner;
//import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import android.view.View.OnClickListener;
import com.facebook.madmathz.connector.*;
import com.facebook.model.GraphUser;

public class LogoView extends Activity implements OnClickListener {
//	Button mFaceBookBtn;
	private Facebook facebook;
	private AsyncFacebookRunner mAsyncRunner;
	SharedPreferences mPrefs;
	FacebookConnector mFacebookConnector;
    final static int AUTHORIZE_ACTIVITY_RESULT_CODE = 1;  //force authentification without sso
//    String[] permissions = { "offline_access", "publish_stream", "user_photos", "publish_checkins", "photo_upload"};
//    String[] permissions = {"publish_stream","email","user_groups","read_stream","user_about_me","offline_access"};
    final static String[] permissions = {"publish_stream", "email"};
    private ImageButton mFaceBookBtn;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_logo_view);
		//Facebook initialization
		Preferences.setActivity(this);
		Preferences.setContext(getApplicationContext());
		mFaceBookBtn = (ImageButton)findViewById(R.id.mBtnFind);
		mFaceBookBtn.setOnClickListener(this);
		mFaceBookBtn.setOnTouchListener(new ButtonHighlight(mFaceBookBtn));
		
		

		
        // restore session if one exists


		Intent mIntent = this.getIntent();
		
		String strLogout = "";
		strLogout =	mIntent.getStringExtra("logout");
		
		if(strLogout != null)
		{
			if(strLogout.equals("request_logout"))
			{
				//logout
				clearCredentials();
			}
			
		}
	}
	
	//logout
	public void clearCredentials()
	{
		CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(getApplicationContext());
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();
        Utility.mFacebook.setAccessToken(null);
        Utility.mFacebook.setAccessExpires(0);
		Utility.mAsyncRunner.logout(getApplicationContext(), new RequestListener(){

			@Override
			public void onComplete(String response, Object state) {
				// TODO Auto-generated method stub
				/*
				CookieSyncManager cookieSyncMngr = 
				CookieSyncManager.createInstance(getApplicationContext()); 
				CookieManager cookieManager = CookieManager.getInstance(); 
				cookieManager.removeAllCookie(); 
				*/
			}

			@Override
			public void onIOException(IOException e, Object state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onFileNotFoundException(FileNotFoundException e,
					Object state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onMalformedURLException(MalformedURLException e,
					Object state) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onFacebookError(FacebookError e, Object state) {
				// TODO Auto-generated method stub
				
			}});
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		  super.onActivityResult(requestCode, resultCode, data);
		 Utility.mFacebook.authorizeCallback(requestCode, resultCode, data);
//	      Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
	}
	
	final Runnable mUpdateFacebookNotification = new Runnable() {
		public void run() {
			
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_logo_view, menu);
		
		return true;
	}
	
	public void onClick(View v)
	{
		switch(v.getId())
		{
			case R.id.mBtnFind: //Login
			{
				if(! Utility.isNetworkReacheable(this))
				{
					Utility.showToast(this, Preferences.TOAST_NOCONNECT);
					return;
				}
				CookieSyncManager cookieSyncMngr = 
					CookieSyncManager.createInstance(this); 
					CookieManager cookieManager = CookieManager.getInstance(); 
					//cookieManager.removeAllCookie(); 
				  //CookieSyncManager.createInstance(getApplicationContext());
                Utility.mFacebook.authorize(this, permissions,Facebook.FORCE_DIALOG_AUTH, new DialogListener() {
//	                Utility.mFacebook.authorize(this, permissions, new DialogListener() {
	                	                    @Override
                    public void onComplete(Bundle values) {
                    	CookieSyncManager.getInstance().sync(); 
                    	//authentification Sucess
                    	Preferences.access_token = Utility.mFacebook.getAccessToken();
                    	Preferences.access_expires = Utility.mFacebook.getAccessExpires();
                    	Preferences.savePreferences(getApplicationContext());
                    	
                    	Intent intent = new Intent(LogoView.this, MadMathz.class);
                    	startActivity(intent);
                    	LogoView.this.finish();
                    }
          
                    @Override
                    public void onFacebookError(FacebookError error) {}
          
                    @Override
                    public void onError(DialogError e) {}
          
                    @Override
                    public void onCancel() {}
               });
			}

				break;
		
		}
	}
	

	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if(keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_MENU || keyCode == KeyEvent.KEYCODE_HOME)
		{
			stopService(new Intent(this, MediaPlayerService.class));
		}
		return super.onKeyDown(keyCode, event);	
	}
}
