package com.facebook.madmathz;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.android.AsyncFacebookRunner.RequestListener;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class Utility {
	
    public static Facebook mFacebook;
    @SuppressWarnings("deprecation")
    public static AsyncFacebookRunner mAsyncRunner;
    public static JSONObject mFriendsList;
    public static String userUID = null;
    public static String objectID = null;
    public static FriendsGetProfilePics model;
    public static AndroidHttpClient httpclient = null;
    public static Hashtable<String, String> currentPermissions = new Hashtable<String, String>();

    private static int MAX_IMAGE_DIMENSION = 720;
    public static final String HACK_ICON_URL = "http://www.facebookmobileweb.com/hackbook/img/facebook_icon_large.png";
    

	//getBitmap
	public static Bitmap getBitmap(String url) {
        Bitmap bm = null;
        try {
            URL aURL = new URL(url);
            URLConnection conn = aURL.openConnection();
            conn.connect();
            InputStream is = conn.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            bm = BitmapFactory.decodeStream(new FlushedInputStream(is));
            bis.close();
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (httpclient != null) {
                httpclient.close();
            }
        }
        return bm;
    }
	

	

    static class FlushedInputStream extends FilterInputStream {
        public FlushedInputStream(InputStream inputStream) {
            super(inputStream);
        }

        @Override
        public long skip(long n) throws IOException {
            long totalBytesSkipped = 0L;
            while (totalBytesSkipped < n) {
                long bytesSkipped = in.skip(n - totalBytesSkipped);
                if (bytesSkipped == 0L) {
                    int b = read();
                    if (b < 0) {
                        break; // we reached EOF
                    } else {
                        bytesSkipped = 1; // we read one byte
                    }
                }
                totalBytesSkipped += bytesSkipped;
            }
            return totalBytesSkipped;
        }
    }
    
    static Context mContext;
    public static void removeIncomingCalls(RelativeLayout rootView, Context context)
    {
    	LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	View inflatedView = mInflater.inflate(R.layout.phone_call, null);
    	rootView.removeView(inflatedView);
    	
    }
    
    public static void drawIncomingCalls(RelativeLayout rootView, Context context)
    {
    	mContext = context;
    	
    	LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	View inflatedView = mInflater.inflate(R.layout.phone_call, null);
    	inflatedView.setBackgroundColor(0xB2000000);
    	
    	rootView.addView(inflatedView);
    	
    	ImageButton btn_resume = (ImageButton)inflatedView.findViewById(R.id.btn_resume);
    	btn_resume.setOnTouchListener(new ButtonHighlight(btn_resume));
    	btn_resume.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
			}});
    	
    	ImageButton btn_quit = (ImageButton)inflatedView.findViewById(R.id.btn_quit);
    	btn_quit.setOnTouchListener(new ButtonHighlight(btn_quit));
    	
    	btn_quit.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
			}});
    }
    
    public static void removeLoading(RelativeLayout LoadingView)
    {
    	LoadingView.removeAllViews();
    	LoadingView.setVisibility(View.INVISIBLE);
    }
    

	public static void drawLoading(RelativeLayout LoadingView, Context context)
	{
//		RelativeLayout LoadingView = (RelativeLayout)findViewById(resId);
	    LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View inflatedView = mInflater.inflate(R.layout.loadingview, null);

		LoadingView.setVisibility(View.VISIBLE);
		LoadingView.setBackgroundColor(0xB2000000);
		LoadingView.addView(inflatedView);
		
		
		//eye animation
		final ImageView eye_img = (ImageView)inflatedView.findViewById(R.id.eye_img);
		int x = eye_img.getLeft();
		int y = eye_img.getTop();
		final Animation animation = new TranslateAnimation(x-7, x+7, y, y);
		animation.setDuration(500);
		final Animation animation1 = new TranslateAnimation(x+7, x-7, y, y);
		animation1.setDuration(500);
		
		eye_img.startAnimation(animation);
		
	    animation.setAnimationListener(new AnimationListener(){

	        @Override
	        public void onAnimationEnd(Animation arg0) {
	            // start animation2 when animation1 ends (continue)
	        	eye_img.startAnimation(animation1);
	        }

	        @Override
	        public void onAnimationRepeat(Animation arg0) {
	            // TODO Auto-generated method stub

	        }

	        @Override
	        public void onAnimationStart(Animation arg0) {
	            // TODO Auto-generated method stub

	        }

	    });
	    
	    animation1.setAnimationListener(new AnimationListener(){

	        @Override
	        public void onAnimationEnd(Animation arg0) {
	            // start animation2 when animation1 ends (continue)
	        	eye_img.startAnimation(animation);
	        }

	        @Override
	        public void onAnimationRepeat(Animation arg0) {
	            // TODO Auto-generated method stub

	        }

	        @Override
	        public void onAnimationStart(Animation arg0) {
	            // TODO Auto-generated method stub

	        }

	    });
	}
	
	public static boolean isNetworkReacheable(Context context)
	{
	    try{
            ConnectivityManager connectivityManager = (ConnectivityManager)       
                                                                      context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo wifiInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobileInfo = 
                                 connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (wifiInfo.isConnected() || mobileInfo.isConnected()) {
                return true;
            }
        }
        catch(Exception e){
           e.printStackTrace();
        }
	    
        return false;
    }
	
	public static void showToast(Context context, int state)
	{
		Toast toast = Toast.makeText(context, Preferences.str_NOCONNECT, 1000);
		toast.show();
		
	}
    
}
