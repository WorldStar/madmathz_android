package com.facebook.madmathz;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.util.TypedValue;
import android.widget.ImageView;


public class GetPicAsyncTask extends AsyncTask<ImageView, Void, Bitmap> {
     String uid;
     ImageView v;

     @Override
     protected Bitmap doInBackground(ImageView... params) {
    	 
    	 String url = "";
    	 
    	 
    	 try{
        	 this.v = (ImageView)params[0];
             url = (String)v.getTag();
    		 
    	 }
    	 catch(Exception e)
    	 {
    		 System.out.println(e.toString());
    	 }

         return Utility.getBitmap(url);
     }

     @Override
     protected void onPostExecute(Bitmap result) {
         if (result != null) {
         	v.setImageBitmap(getRoundedCornerImage(result));
         	v.invalidate();
         }
     }
     
     public static Bitmap getRoundedCornerImage(Bitmap bitmap) {
    	 Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
    	     bitmap.getHeight(), Config.ARGB_8888);
    	 Canvas canvas = new Canvas(output);

    	 final int color = Preferences.tx_color; //0xff424242;
    	 final Paint paint = new Paint();
    	 final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
    	 final RectF rectF = new RectF(rect);
    	 final float roundPx = 10;

    	 paint.setAntiAlias(true);
    	 canvas.drawARGB(0, 0, 0, 0);
    	 paint.setColor(color);
    	 canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

    	 paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
    	 canvas.drawBitmap(bitmap, rect, rect, paint);
    	 
    	 //border
    	 paint.setColor(Preferences.tx_color);
    	 paint.setStyle(Paint.Style.STROKE);
    	 paint.setStrokeWidth(2);
    	 canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

    	 return output;
   	 }
 }